//
//  ClassesAndStructs.swift
//  SuperheroesLaureates
//
//  Created by Keenan Piveral-Brooks on 4/13/19.
//  Copyright © 2019 Keenan Piveral-Brooks. All rights reserved.
//

import Foundation

struct Superhero: Codable {
	var name: String
	var age: Int
	var secretIdentity: String
	var powers: [String]
}

struct Squad: Codable {
	let members: [Superhero]
}

struct Laureate: Codable {
	let firstName: String?
	let surName: String?
	let born: String
	let died: String
}
