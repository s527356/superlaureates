//
//  LaureatesTableViewController.swift
//  SuperheroesLaureates
//
//  Created by Keenan Piveral-Brooks on 4/13/19.
//  Copyright © 2019 Keenan Piveral-Brooks. All rights reserved.
//

import UIKit

class LaureatesTableViewController: UITableViewController {
	
	var laureateInfo = "https://www.dropbox.com/s/7dhdrygnd4khgj2/laureates.json?dl=1"
	
	override func viewDidLoad() {
		super.viewDidLoad()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		if laureates.isEmpty {
			URLSession.shared.dataTask(with: URL(string: laureateInfo)!, completionHandler: showLaureates)
		}
	}
	
	func showLaureates(data: Data?, urlResponse: URLResponse?, error: Error?) {
		laureates = try! JSONDecoder().decode([Laureate].self, from: data!)
		DispatchQueue.main.async {
			self.tableView.reloadData()
		}
	}
	
	var laureates: [Laureate] = []
	
	override func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}
	
	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return laureates.count
	}
	
	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "laureate", for: indexPath)
		cell.textLabel?.text = "\(laureates[indexPath.row].firstName ?? "") \(laureates[indexPath.row].surName ?? "")"
		cell.detailTextLabel?.text = "\(laureates[indexPath.row].born == "0000-00-00" ? "Unknown" : laureates[indexPath.row].born) - \(laureates[indexPath.row].died == "0000-00-00" ? "Present" : laureates[indexPath.row].died)"
		return cell
	}
}
