//
//  HeroesTableViewController.swift
//  SuperheroesLaureates
//
//  Created by Keenan Piveral-Brooks on 4/13/19.
//  Copyright © 2019 Keenan Piveral-Brooks. All rights reserved.
//

import UIKit

class HeroesTableViewController: UITableViewController {
	
	let heroInfo = "https://www.dropbox.com/s/wpz5yu54yko6e9j/squad.json?dl=1"
	
	override func viewWillAppear(_ animated: Bool) {
		if superheros.isEmpty {
			URLSession.shared.dataTask(with: URL(string: heroInfo)!, completionHandler: showSuperheros)
		}
	}
	
	func showSuperheros(data: Data?, urlResponse: URLResponse?, error: Error?) {
		superheros = try! JSONDecoder().decode(Squad.self, from: data!).members
		DispatchQueue.main.async {
			self.tableView.reloadData()
		}
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
	}
	
	var superheros: [Superhero] = []
	
	override func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}
	
	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return superheros.count
	}
	
	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "superhero", for: indexPath)
		cell.textLabel?.text = "\(superheros[indexPath.row].name) (aka: \(superheros[indexPath.row].secretIdentity)"
		cell.detailTextLabel?.text = "\(superheros[indexPath.row].powers.joined(separator: ", "))"
		return cell
	}
}
